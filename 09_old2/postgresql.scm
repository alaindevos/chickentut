;(row-fold (lambda (row sum) (+ (car row) sum))
;(query conn "SELECT 1 UNION SELECT 2")))
(import simple-loops)
(import postgresql)
(define conn
	(connect '((host . "127.0.0.1")
                     (user . "syslogng")
                     (password . "x")
                     (dbname . "syslogng"))
             );connect
    );define
(define selstring
	"select * from messages_rasp_20230117 order by datetime desc"
    );define
(define q
  (query conn selstring)
  );define 4

(define printrow
  (lambda (x)
    {begin
            (write x)
            (newline)
            };begin
            );lambda
  );define
(row-for-each
  printrow
  q
  );for
