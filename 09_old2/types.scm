(: xxx integer )
(define xxx
  123)

(: myfun (integer -> integer ))
(define myfun
  (lambda (x)
    (+ x 1 )))

(: y list)
(define y '(1 2 3 ))
