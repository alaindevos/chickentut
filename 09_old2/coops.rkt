(import utf8)
(import coops)
(define-class <human> () (name birthdate children))
(define-class <male> (<human>) (testosterone-level))
(define-class <female> (<human>) (estrogen-level))
(define-generic (have-sex! a b))
(define-method (have-sex! (a <male>) (b <female>))
               (let ((newborn (make <human> 'name "Bobby" 'birthdate (current-seconds) 'children '())))
                    (set! (slot-value a 'children) (cons newborn (slot-value a 'children)))
                    (set! (slot-value b 'children) (cons newborn (slot-value b 'children)))))
(define-method (have-sex! (a <male>) (b <male>)) 'just-fun)
(define-method (have-sex! (a <female>) (b <female>)) 'just-fun)

(define aman (make <male>
              'name "John"
              'birthdate "Today"
              'children '()
              ))
(define awomen (make <male>
              'name "Elise"
              'birthdate "Today"
              'children '()
              ))
(have-sex! aman awomen)

