(use matchable coops linenoise)

(define-class <player> ()
  ((description initform: "You don't see anything special")
   (location initform: '())
   (inventory initform: '())))

(define-class <object> ()
  ((name)
   (description initform: "You don't see anything special.")
   (movable initform: '#t)
   (weight initform: 1)
   (close-description "You don't see anything special.")))

(define-class <container> (<object>)
  ((transparent '#f)
   (status 'closed)
   (contents '())))

(define-class <room> ()
  ((name)
   (long-description '())
   (objects '())))


(define (say . things)
  (for-each display things)
  (blank))

(define (blank)
  (newline))

(define direction cadr)
(define direction-name (lambda (x) (slot-value (caddr x) 'name)))

(define-generic (describe <room>))
(define-generic (examine <object>))
(define-generic (take <object> <player>))
(define-generic (go <player> direction))
(define-generic (connect <room1> <room2>))
(define-generic (get-exits <room>))
(define-generic (print-inventory <player>))
(define-generic (drop <object>))
(define-generic (carrying? <object> <player>))


(define-method (carrying? (o <object>) (p <player>))
  (not (null? (member o (slot-value p 'inventory)))))

(define-method (print-inventory (p <player>))
  (say "You are carrying:")
  (let ((things (slot-value me 'inventory)))
    (if (null? things) (say "\tnothing.")
        (for-each (lambda (thing) (say "\t" (slot-value thing 'name))) things))))

(define-method (get-exits (r <room>))
  (filter (lambda (e)
            (equal? (slot-value (car e) 'name) (slot-value r 'name)))
          *rooms*))

(define-method (describe (location <room>))
  (let ((location-name (slot-value location 'name)))
    (blank)
    (say (slot-value location 'long-description))
    (when (not (null? (slot-value location 'objects)))
          (blank)
          (say "You see " (fold (lambda (element seed) (string-append seed "a " (slot-value element 'name) " ")) "" (slot-value location 'objects)) "."))
    (cond ((get-exits location) =>
           (lambda (exits)
            (blank)
            (say "There are exits to")
            (for-each (lambda (e) (say "\tthe " (symbol->string (direction e)) ", leading to the " (direction-name e))) exits))))))


(define-method (examine (o <object>))
  (say (slot-value o 'close-description)))

(define-method (take (o <object>) (p <player>))
  (let ((location (slot-value p 'location)))
    (if (not (slot-value o 'movable))
        (say "I cannot take " (slot-value o 'name))
        (begin
          (set! (slot-value p 'inventory) (cons o (slot-value p 'inventory)))
          (set! (slot-value location 'objects) (remove (lambda (obj) (eq? o obj)) (slot-value location 'objects)))
          (say "Taken.")))))

(define-method (drop (o <object>) (p <player>))
  (let ((location (slot-value p 'location)))
    (if (not (carrying? o p))
        (say "You don't have " (slot-value 'name o))
        (begin
          (set! (slot-value location 'objects) (cons o (slot-value location 'objects)))
          (set! (slot-value p 'inventory) (remove (lambda (obj) (eq? o obj)) (slot-value p 'inventory)))
          (say "Dropped " (slot-value o 'name))))))

(define-method (print-object (o <object>) p)
  (fprintf p "#<'<object>', a ~A>" (slot-value o 'name)))

(define-method (print-object (r <room>) p)
  (fprintf p "#<'<room>, the ~A>" (slot-value r 'name)))

;; Rooms are connected in this list:
;; room1 direction target-room

(define *rooms* '())
(define-method (connect (room1 <room>) (room2 <room>) direction1 direction2)
  (let ((exits-room1 (alist-ref room1 *rooms*))
        (exits-room2 (alist-ref room2 *rooms*)))
    (set! *rooms* (cons (list room1 direction1 room2) *rooms*))
    (set! *rooms* (cons (list room2 direction2 room1) *rooms*))))


(define-method (go (p <player>) direction)
  (or (and-let* ((location (slot-value p 'location))
                 (exits (get-exits location))
                 (my-exit (alist-ref (string->symbol direction) (map cdr exits) equal?)))
                (set! (slot-value p 'location)  (car my-exit))
                (say (slot-value (slot-value  p 'location) 'name))
                (describe (slot-value p 'location)))
      (say "You cannot go " direction)))


(define (opposite direction)
  (match direction
         ('north 'south)
         ('south 'north)
         ('east 'west)
         ('west 'east)
         ('northwest 'southeast)
         ('southeast 'northwest)
         ('northeast 'southwest)
         ('southwest 'northeast)))

(define-syntax place
  (syntax-rules (of)
    ((_ room1 direction of room2)
     (connect room1 room2 (opposite (quote direction)) (quote direction)))))


;; interpreter functions follow

(define (fetch-objects name list)
  (filter (lambda (e)
            (equal? (slot-value e 'name) name)) list))

(define (execute-examine o)
  (let* ((loc (slot-value me 'location))
         (obj-inventory (fetch-objects o (slot-value me 'inventory)))
         (obj-room (fetch-objects o (slot-value loc 'objects))))
    (cond ((not (null? obj-room))
          (for-each (lambda (o) (examine o)) obj-room))
          ((not (null? obj-inventory))
           (for-each (lambda (o) (examine o)) obj-inventory))
          (else (say "I cannot see " o)))))


(define (execute-take o)
  (let* ((loc (slot-value me 'location))
         (obj (fetch-objects o (slot-value loc 'objects))))
    (if (null? obj)
        (say "There is no " o " here.")
        (for-each (lambda (obj) (take obj me)) obj))))

(define (execute-drop o)
  (let ((objects (fetch-objects o (slot-value me 'inventory))))
    (if (null? objects)
        (say "You don't have " o)
        (for-each (lambda (o) (drop o me)) objects))))

(define (execute-go d)
  (go me d))

(define (execute-describe l)
  (describe l))

(define (execute-inventory)
  (print-inventory me))

(define (prompt)
  (or (and-let* ((location (slot-value me 'location))
                 (request (linenoise "> "))
                 (commands (string-split request)))
                (match commands
                       (("examine" o) (execute-examine o))
                       (("take" o) (execute-take o))
                       (("drop" o) (execute-drop o))
                       (("go" d) (execute-go d))
                       (("look at" o) (execute-examine o))
                       (("look") (execute-describe location))
                       (("inventory") (execute-inventory))
                       (_ (say "I cannot make sense of this.")))
                (prompt))
      (begin
        (say "Bye!")
        (exit 0))))


;;; game world follows

(define table (make <object>
                'name "table"
                'description "A wooded kitchen table, very clean"
                'close-description "There seems to be a small message carved on the side"
                'movable #f))

(define apple (make <object>
                'name "apple"
                'description "A juicy looking red delicious"
                'close-description "You cannot find a wormhole, it is a spotless apple!"
                'movable #t))

(define living-room (make <room>
                      'name "living room"
                      'long-description "Your main room in your home contains a comfy sofa, a book shelf and needs some cleaning after last night. A pile of clothes is stacked on the floor in front of the sofa and they do not look like yours.
A door to the south leads into the kitchen."
                      'objects '()))

(define kitchen (make <room>
                  'name "kitchen"
                  'long-description "You are standing in the cleanest kitchen you have ever seen. A stove, sink, kitchen table and a cupboard with cups and plates fill up the small room. On the table sits an apple"
                  'objects `(,table ,apple)))

(define bedroom (make <room>
                  'name "bedroom"
                  'long-description "Everyone tells you that this room resembles the stereotype of a bachelor's bedroom. Posters of lightly dressed celebrities hang at one wall, towers of laundry pile up beside your unmade bed."))

(place living-room north of kitchen)
(place bedroom east of living-room)

(define me (make <player>
             'description "A normal looking guy."
             'location kitchen))

(blank)
(describe (slot-value me 'location))
(prompt)
