(import coops)
(import coops-primitive-objects)

(define-class <stack> ()
  ((content initform: '()))
  );define

(define-method (push (val #t) (stack <stack>))
  (set! (slot-value stack 'content)
    (cons val (slot-value stack 'content))))

(define-method (pop (stack <stack>))
  (let* ((c (slot-value stack 'content)) (x (car c))  (y (cdr c)))
    (set! (slot-value stack 'content) y )
    x))

(define-method (empty? (stack <stack>))
  (let* ((c (slot-value stack 'content)))
    (null? c)))

(define astack (make <stack>))
(push 0 astack)
(push 1 astack)
(write (pop astack))
(write (pop astack))
