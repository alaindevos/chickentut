(require-extension typed-records)
(require-extension matchable)

(define-record my2dpoint [ x2 : integer ]
                         [ y2 : integer ]
                     )

(define-record my3dpoint [ x3 : integer ]
                         [ y3 : integer ]
                         [ z3 : integer ]
                         )

(define-type myunion (or  (struct my2dpoint) (struct my3dpoint) ))


( : myfirst ( myunion -> integer ))
(define myfirst
  (lambda (x)
    [match x
      ([ $ my2dpoint x2 y2 ] x2)
      ([ $ my3dpoint x3 y3 z3] x3)
      ];match
    );lambda
  );define


(define a (make-my2dpoint 1 2 ))
(write (myfirst a))
