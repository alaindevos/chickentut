(define (fib x)
    (if (< x 2) 
        1
        (+ (fib (- x 2)) 
           (fib (- x 1)))))

(display (fib 39))
