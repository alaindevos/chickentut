(import simple-loops)
(import postgresql)
(define conn
	(connect '((host . "127.0.0.1")
                     (user . "x")
                     (password . "x")
                     (dbname . "x"))
             );connect
    );define
(define selstring
	"select firstname,lastname from person"
    );define
(define q
  (query conn selstring)
  );define 4

(define printrow
  (lambda (x)
    {begin
            (write x)
            (newline)
            };begin
            );lambda
  );define
(row-for-each
  printrow
  q
  );for
