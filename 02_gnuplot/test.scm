(import (prefix gnuplot-pipe gp:))
(gp:call/gnuplot
 (gp:send "unset key")
 (gp:send "set style data points")
 (gp:send "set title 'The valley of the Gnu'")
 (gp:plot3d ""
            '(0 0 0 1 1 1 2 2 2 3 3 3)
            '(0 1 2 0 1 2 0 1 2 0 1 2)
            '(10 10 10 10 5 10 10 1 10 10 0 10)))
