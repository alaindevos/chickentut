(import simple-loops)
(import postgresql)
(import pstk)
(define conn
	(connect '((host . "127.0.0.1")
                     (user . "x")
                     (password . "x")
                     (dbname . "syslogng"))
             );connect
    );define
(define selstring
	"select * from messages_freebsd_20230118 order by datetime desc"
    );define
(define q
  (query conn selstring)
  );define

(tk-start)
(ttk-map-widgets 'all) ; make sure we are using tile widget set
(tk/wm 'title tk "PS-Tk Example: TreeView")
;(tk 'configure 'height: 800 'width: 1000)
(let* ((treeview (tk 'create-widget 'treeview 'columns: '("col1" "col2" "col3" "col4" "col5")))
      (hsb (tk 'create-widget 'scrollbar 'orient: 'horizontal))
      (vsb (tk 'create-widget 'scrollbar 'orient: 'vertical))
      (counter 0)
      (counterstring "")
      (mycol
        (lambda (myname mywidth)
          {begin
            (treeview 'column myname 'width: mywidth 'minwidth: mywidth 'stretch: #t)
            (treeview 'heading myname 'text: myname)
            }
            );lambda
        );mycol
      (dorow
        (lambda (r)
          {begin
            (set! counter (+ 1 counter))
            (set! counterstring (number->string counter))
            (treeview 'insert "" 'end 'id: counterstring 'text: counterstring 'values: r)
            }
            );lambda
        );dorow
      );letdef
  (hsb 'configure 'command: (list treeview 'xview))
  (vsb 'configure 'command: (list treeview 'yview))
  (treeview 'configure 'xscrollcommand: (list hsb 'set))
  (treeview 'configure 'yscrollcommand: (list vsb 'set))
  (mycol "col1" 40)
  (mycol "col2" 20)
  (mycol "col3" 20)
  (mycol "col4" 20)
  (mycol "col5" 100)
  (row-for-each dorow q );for
  (treeview 'configure 'height: 40 )
  (tk/grid treeview 'column: 0 'row: 0 'sticky: 'news 'columnspan: 40 'rowspan: 40 )
  (tk/grid hsb 'column: 0 'row: 1 'sticky: 'we)
  (tk/grid vsb 'column: 1 'row: 0 'sticky: 'ns)
  (tk/grid 'columnconfigure tk 0 'weight: 1)
  (tk/grid 'rowconfigure    tk 0 'weight: 1)
  );let
(tk-event-loop)
