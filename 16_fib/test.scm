(: fib (fixnum -> fixnum))

(define (fib n)
  (if (> n 2)
      (+ (fib(- n 1)) (fib(- n 2)))
      n))

(display (fib 44))
